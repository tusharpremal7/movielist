//
//  CustomClass.swift
//  MovieList
//
//  Created by Tushar Premal on 20/05/20.
//  Copyright © 2020 Tushar Premal. All rights reserved.
//

import Foundation
import UIKit

class CusomClass: NSObject {
    
    static let shared = CusomClass()
    
    func saveJsonIntoDirectory(_ jsonData:Data){
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Movies.json")
        do {
            try jsonData.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    
    func getJsonFromDirectioner()->[MovieViewModel]{
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return [MovieViewModel]() }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Movies.json")
        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            return parseJSON(data)
        } catch {
            print(error)
        }
        return [MovieViewModel]()
    }
    
    func parseJSON(_ jsondata:Data)->[MovieViewModel]{
        var error: NSError?
        var response:[MovieModel]?
        do {
            response = try JSONDecoder().decode([MovieModel].self, from: jsondata)
            //parsing data into codable struct
        }catch let error1 as NSError {
            error = error1
            response = nil
        } catch {
            fatalError()
        }
        if (error != nil){
            print(error!.localizedDescription)
        }else{
            if response != nil{
                //setting codable struct to journal view model class
                var arrModel = [MovieViewModel]()
                for result in response!{
                    arrModel.append(MovieViewModel.init(result))
                }
                return arrModel
            }
        }
        return [MovieViewModel]()
    }
    
    func showAlertView(_ title : String?, _ message : String, _ buttons : [String], completionHandler : @escaping ((_ index : Int) -> Void)) {
        DispatchQueue.main.async {
            //create alertview
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            for (index, _) in buttons.enumerated() {
                alertViewController.addAction(UIAlertAction(title:  buttons[index], style: .default, handler: { (selectedButtonIndex) in completionHandler(index) }))
            }
            //get top view controller
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
}

class MovieViewCell:UITableViewCell{
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblGenre:UILabel!
    @IBOutlet weak var imgMovie:UIImageView!
    @IBOutlet weak var vwRating:EZRatingView!
    
    
    override func awakeFromNib() {
        self.selectionStyle = .none
    }
    
}
