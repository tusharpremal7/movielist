//
//  ViewController.swift
//  MovieList
//
//  Created by Tushar Premal on 20/05/20.
//  Copyright © 2020 Tushar Premal. All rights reserved.
//

import UIKit
import JGProgressHUD
import Alamofire

class MovieListVC: UIViewController {

    @IBOutlet weak var tblMovie:UITableView!
    var arrMovieList = [MovieViewModel]()
    var refreshControl:UIRefreshControl!
    let hud = JGProgressHUD(style: .dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.arrMovieList = CusomClass.shared.getJsonFromDirectioner()
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.getMovieData), for: .valueChanged)
        tblMovie.addSubview(refreshControl)
        getMovieData()
    }
    
    func showLoader(){
        hud.textLabel.text = "Loading"
        incrementHUD(0)
        hud.show(in: self.view)
    }
    
    func incrementHUD(_ previousProgress: Int) {
        let progress = previousProgress + 1
        hud.progress = Float(progress)/100.0
        hud.detailTextLabel.text = "\(progress)% Complete"
        if progress == 100 {
            self.hud.dismiss()
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(20)) {
                self.incrementHUD(progress)
            }
        }
    }
    
    @objc func getMovieData(){
        let reachability = Reachability.forInternetConnection()
        if reachability!.isReachableViaWiFi() || reachability!.isReachableViaWWAN(){
            showLoader()
            let request = AF.request("http://api.androidhive.info/json/movies.json", method: .post)
            request.responseDecodable(of: [MovieModel].self) { (response) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.hud.dismiss(animated: true)
                }
                self.refreshControl.endRefreshing()
                if response.error != nil{
                    //print(response.error!.localizedDescription)
                    CusomClass.shared.showAlertView(nil, response.error!.localizedDescription, ["OK"]) { (index) in
                        
                    }
                }else{
                    
                    guard let movies = response.value else {
                        CusomClass.shared.showAlertView(nil, "Failed to get movie data.", ["OK"]) { (index) in
                            
                        }
                        return
                    }
                    self.arrMovieList.removeAll()
                    for result in movies{
                        self.arrMovieList.append(MovieViewModel.init(result))
                    }
                    self.tblMovie.reloadData()
                }
            }
        }else{
            CusomClass.shared.showAlertView(nil, "Internet connection is not available.", ["OK"]) { (index) in
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.refreshControl.endRefreshing()
            }
            
        }
    }
}

extension MovieListVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMovieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieViewCell", for: indexPath) as! MovieViewCell
        arrMovieList[indexPath.row].setCellData(cell)
        return cell
    }
}
