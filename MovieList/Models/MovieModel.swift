//
//  JournalModel.swift
//  MovieList
//
//  Created by Tushar Premal on 19/05/20.
//  Copyright © 2020 Tushar Premal. All rights reserved.
//

import Foundation

struct MovieModel: Codable{
    
    var title:String?
    var image:String?
    var genre:[String]?
    var rating:Double?
    
}


