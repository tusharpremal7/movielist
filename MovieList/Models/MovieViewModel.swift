//
//  JournalViewModel.swift
//  MovieList
//
//  Created by Tushar Premal on 19/05/20.
//  Copyright © 2020 Tushar Premal. All rights reserved.
//

import Foundation
import AlamofireImage

class MovieViewModel {
    
    private let movieModel:MovieModel!
    
    init(_ model: MovieModel){
        self.movieModel = model
    }
    
    public var title: String {
        return movieModel.title ?? ""
    }
    
    public var imageURL: String {
        return movieModel.image ?? ""
    }
    
    public var rating: CGFloat {
        return CGFloat(movieModel.rating ?? 0)/2
    }
    
    public var genre: String {
        var strGenre = ""
        if movieModel.genre != nil{
            for str in movieModel.genre!{
                if strGenre == ""{
                    strGenre = str
                }else{
                    strGenre = "\(strGenre), \(str)"
                }
            }
        }
        return strGenre
    }
    
    func setCellData(_ cell:MovieViewCell){
        cell.lblTitle.text = self.title
        cell.lblGenre.text = self.genre
        cell.vwRating.value = self.rating
        cell.imgMovie.image = UIImage.init(named: "placeholder")
        if let url = URL.init(string: self.imageURL){
            cell.imgMovie.af.setImage(withURL: url, cacheKey: nil, placeholderImage: UIImage.init(named: "placeholder"), serializer: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: true, completion: nil)
        }
    }
}
